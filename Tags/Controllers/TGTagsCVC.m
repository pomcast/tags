//
//  TGTagsCVC.m
//  Tags
//
//  Created by Manuel "StuFF mc" Carrasco Molina on 10/26/13.
//  Copyright (c) 2013 Manuel "StuFF mc" Carrasco Molina. All rights reserved.
//

#import "TGTagsCVC.h"
#import "TGCollectionViewCell.h"

@interface TGTagsCVC ()

@end

@implementation TGTagsCVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TGCollectionViewCell *cvCell = (TGCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cvCell" forIndexPath:indexPath];
    cvCell.backgroundColor = [UIColor grayColor];
    [cvCell.tagButton setTitle:indexPath.row ? @"aaa" : @"bbb"
                      forState:UIControlStateNormal];
    return cvCell;
}


@end
