//
//  TGViewController.m
//  Tags
//
//  Created by Manuel "StuFF mc" Carrasco Molina on 10/26/13.
//  Copyright (c) 2013 Manuel "StuFF mc" Carrasco Molina. All rights reserved.
//

#import "TGViewController.h"

@interface TGViewController ()

@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UITableView *searchTV;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation TGViewController {
    NSMutableDictionary *taggedTextRanges;
    NSString *fullContentTextView;
    NSMutableSet *selectedTags;
    NSArray *tags;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%d", _contentTextView.text.length);
    taggedTextRanges = [@{[NSValue valueWithRange:NSMakeRange(0, 30)] : @"aaa"} mutableCopy];
    selectedTags = [NSMutableSet set];
//    tags = @[@"aaa", @"bbb"];
    [_contentTextView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"%@", searchText);
    NSLog(@"%@", taggedTextRanges);
    // Look for the tag — TODO: Maybe use a for or for in loop, faster than enumerate
    fullContentTextView = _contentTextView.text;
    [taggedTextRanges enumerateKeysAndObjectsUsingBlock:^(NSValue *key, NSString *obj, BOOL *stop) {
        NSLog(@"%@", obj);
        if ([obj isEqualToString:searchText]) {
            _contentTextView.text = [_contentTextView.text substringWithRange:key.rangeValue];
        }
        
        if ([obj rangeOfString:searchText].location != NSNotFound) {
            tags = @[obj];
            _searchTV.hidden = NO;
            [_searchTV reloadData];
        }
    }];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    _searchTV.hidden = YES;
    _contentTextView.text = fullContentTextView;
    [_contentTextView becomeFirstResponder];
}


- (IBAction)addTag:(UIButton*)sender {
    // The following is (given) super stupid :-) We should find a better way to know if we are on off or on mode :)
    BOOL isOn = sender.backgroundColor == [UIColor whiteColor];

    // Adding the object
    if (isOn) {
        [selectedTags addObject:sender.titleLabel.text];
        NSLog(@"%@", selectedTags);
    } else {
        // this doesn't really cope when you have 2 tags for a text — you should find a way to "finish"
        // Finishing the text
        taggedTextRanges[[NSValue valueWithRange:NSMakeRange(40, 20)]] = sender.titleLabel.text;
    }
}


#pragma mark Table View DataSource

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    NSLog(@"%d", selectedTags.count);
    return tags.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    cell.textLabel.text = tags[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _searchBar.text = tags[indexPath.row];
    [self searchBar:_searchBar textDidChange:_searchBar.text];
    _searchTV.hidden = YES;
}



@end
