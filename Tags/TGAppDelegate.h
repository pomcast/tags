//
//  TGAppDelegate.h
//  Tags
//
//  Created by Manuel "StuFF mc" Carrasco Molina on 10/26/13.
//  Copyright (c) 2013 Manuel "StuFF mc" Carrasco Molina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
